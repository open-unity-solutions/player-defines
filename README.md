## About

C# wrapper around scripting defines, that allow to safely set defines:

```csharp
bool isDevEnviroment = false;
PlayerDefines.Set(BuildTargetGroup.Standalone, "IS_DEV", isDevEnviroment);
```


## TODO
- [ ] doc DefineFastAccess
