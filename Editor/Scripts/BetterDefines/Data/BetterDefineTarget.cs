using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;


namespace Valhalla.Automation.Editor.BetterDefines
{
	[SerializeField]
	public class BetterDefineTarget
	{
		[ShowInInspector, ReadOnly]
		private BuildTargetGroup _buildTarget;
		
		
		public BetterDefineTarget(BuildTargetGroup buildTarget)
		{
			_buildTarget = buildTarget;
		}
		
		
		[Button, HorizontalGroup()]
		private void Enable()
		{
			Debug.Log("Enable");
		}


		[Button, HorizontalGroup()]
		private void Disable()
		{
			Debug.Log("Disable");
		}
	}
}
