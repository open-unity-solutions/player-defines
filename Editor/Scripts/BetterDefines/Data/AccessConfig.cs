using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;


namespace Valhalla.Automation.Editor.BetterDefines
{
	[HideMonoScript]
	[CreateAssetMenu(fileName = "better_defines", menuName = "Valhalla/Better Defines/Access Config")]
	public class AccessConfig : ScriptableObject
	{
		[BoxGroup("Input")]
		
		[SerializeField, Required]
		[HorizontalGroup("Input/Row")]
		private List<string> _defines = new();


		[SerializeField, Required]
		[HorizontalGroup("Input/Row")]
		private List<BuildTargetGroup> _targets = new();


		public List<string> Defines
			=> _defines;
		
		
		public List<BuildTargetGroup> Targets
			=> _targets;
	}
}
