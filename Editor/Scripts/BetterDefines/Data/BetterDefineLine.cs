using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;


namespace Valhalla.Automation.Editor.BetterDefines
{
	[Serializable]
	public class BetterDefineLine
	{
		[SerializeField, HorizontalGroup()]
		private string _define = "";


		[ShowInInspector, ListDrawerSettings(IsReadOnly = true)]
		private List<BetterDefineTarget> _buildTargets = new();
		
		
		public void Setup(List<BuildTargetGroup> targets)
		{
			_buildTargets = targets
				.Select(target => new BetterDefineTarget(target))
				.ToList();
		}
		
	}
}
