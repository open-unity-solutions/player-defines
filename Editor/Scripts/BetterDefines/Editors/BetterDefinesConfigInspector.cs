using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using Valhalla.DisposableLayouts.Editor;
using Valhalla.EveryDay.Extensions;


namespace Valhalla.Automation.Editor.BetterDefines
{
	[CustomEditor(typeof(AccessConfig))]
	public class BetterDefinesConfigInspector : OdinEditor
	{

		public override void OnInspectorGUI()
		{
			base.DrawDefaultInspector();
			
			var defineConfigs = (AccessConfig) target;
			
			var defines = new List<string>(defineConfigs.Defines);
			var buildTargets = new List<BuildTargetGroup>(defineConfigs.Targets);
			
			defines.RemoveAll(string.IsNullOrEmpty);
			buildTargets.RemoveAll(buildTarget => buildTarget == BuildTargetGroup.Unknown);
			
			if (defines.Count == 0 || buildTargets.Count == 0)
				return;
			
			var possibleBuildTargets = Enum<BuildTargetGroup>
				.Values
				.Select(buildTarget => buildTarget.ToString())
				.ToArray();

			using (var _ = EditorGUILayoutVertical.Create())
			{
				EditorGUILayout.Space(30);
				
				using (var horizontalTargets = EditorGUILayoutHorizontal.Create())
				{
					using (var firstVertical = EditorGUILayoutVertical.Create())
					{
						EditorGUILayout.Space(20);
						
						foreach (var define in defines)
						{
							EditorGUILayout.TextField(define);
						}
					}
					
					
					foreach (var targetGroup in buildTargets)
					{
						var index = Enum<BuildTargetGroup>
							.Values
							.ToList()
							.IndexOf(targetGroup);

						using (var verticalUnderBuildTarget = EditorGUILayoutVertical.Create())
						{
							EditorGUILayout.Popup(index, possibleBuildTargets);

							foreach (var define in defines)
							{
								var previousValue = PlayerDefines.Get(targetGroup, define);
								var newValue = EditorGUILayout.Toggle(previousValue);
								
								if (newValue != previousValue)
									PlayerDefines.Set(targetGroup, define, newValue);
							}
						}
					}
				}
			}
		}
	}
}
