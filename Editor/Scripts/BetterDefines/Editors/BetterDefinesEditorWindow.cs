using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using Valhalla.AssetManagement.Editor;


namespace Valhalla.Automation.Editor.BetterDefines
{
	public class BetterDefinesEditorWindow : OdinEditorWindow
	{
		private const string DEFAULT_CONFIG_DIRECTORY_PATH = "Assets/Plugins/Valhalla/Player Defines";
		private const string DEFAULT_CONFIG_NAME = "better_defines";

		[ShowInInspector]
		[HideLabel]
		[InlineEditor(InlineEditorObjectFieldModes.Boxed, Expanded = true, ObjectFieldMode = InlineEditorObjectFieldModes.Boxed)]
		private AccessConfig _defines;
		
		
		[MenuItem("Tools/Defines Fast Access")]
		private static void OpenWindow()
		{
			var window = GetWindow<BetterDefinesEditorWindow>();

			window.position = GUIHelper.GetEditorWindowRect().AlignCenter(550, 400);
			window.Init();
		}
		
		
		[UnityEditor.Callbacks.DidReloadScripts]
		private static void OnScriptsReloaded()
		{
			if (HasOpenInstances<BetterDefinesEditorWindow>())
				GetWindow<BetterDefinesEditorWindow>(false);
		}
		
		
		private void Init()
		{
			if (_defines == null)
				_defines = AssetDB.GetAnyOrCreateAtPath<AccessConfig>(DEFAULT_CONFIG_DIRECTORY_PATH, DEFAULT_CONFIG_NAME);
		}
	}
}
