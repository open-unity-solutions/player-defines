using System.Linq;
using UnityEditor;
using UnityEditor.Build;
using UnityEngine;


namespace Valhalla.Automation.Editor
{
	public static class PlayerDefines
	{
		public static void Set(BuildTargetGroup targetGroup, string define, bool value)
		{
			if (value)
				Add(targetGroup, define);
			else 
				Remove(targetGroup, define);
		}


		public static bool Get(BuildTargetGroup targetGroup, string define)
		{
			var target = NamedBuildTarget.FromBuildTargetGroup(targetGroup);
			var defines = PlayerSettings
				.GetScriptingDefineSymbols(target)
				.Split(';')
				.ToList();

			return defines.Contains(define);
		}
		
		public static void Add(BuildTargetGroup targetGroup, string define)
		{
			var target = NamedBuildTarget.FromBuildTargetGroup(targetGroup);

			if (Get(targetGroup, define))
			{
				Debug.LogWarning($"Selected build target ({target}) already contains <b>{define}</b> <i>Scripting Define Symbol</i>, can't add.");            
				return;
			}
			
			string definesLine = PlayerSettings.GetScriptingDefineSymbols(target);

			definesLine += ";" + define;
			
			PlayerSettings.SetScriptingDefineSymbols(target, definesLine);
			Debug.Log($"<b>{define}</b> added to <i>Scripting Define Symbols</i> for selected build target ({target}).");
		}


		public static void Remove(BuildTargetGroup targetGroup, string define)
		{
			var target = NamedBuildTarget.FromBuildTargetGroup(targetGroup);
			string defines = PlayerSettings.GetScriptingDefineSymbols(target);

			if ( !defines.Contains(define))
			{
				Debug.LogWarning($"Selected build target ({target}) does not contain <b>{define}</b> <i>Scripting Define Symbol</i>, can't remove.");            
				return;
			}

			defines = defines
				.Replace(define, "")
				.Replace(";;", ";")
				.Trim(';');
			
			PlayerSettings.SetScriptingDefineSymbols(target, defines);
			Debug.Log($"<b>{define}</b> removed to <i>Scripting Define Symbols</i> for selected build target ({target}).");
		}
	}
}
